#ifndef __RDCP_REALMDREALM_H__
#define __RDCP_REALMDREALM_H__

#include <strings.h>
#include "LMI_RealmdRealm.h"
#include "LMI_RealmdKerberosRealm.h"


KINLINE LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_Enum
SupportedJoinCredentialTypes_name_to_enum(const char *name)
{
    if (strcasecmp(name, "ccache"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_ccache;
    if (strcasecmp(name, "password"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_password;
    if (strcasecmp(name, "secrect"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_secrect;
    if (strcasecmp(name, "automatic"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_automatic;
    return 0;
}

KINLINE const char *
SupportedJoinCredentialTypes_enum_to_name(LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_Enum value)
{
    switch(value) {
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_ccache:
        return "ccache";
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_password:
        return "password";
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_secrect:
        return "secrect";
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_automatic:
        return "automatic";
    default:
        return NULL;
    }
}

KINLINE LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_Enum
SupportedJoinCredentialOwners_name_to_enum(const char *name)
{
    if (strcasecmp(name, "administrator"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_administrator;
    if (strcasecmp(name, "user"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_user;
    if (strcasecmp(name, "computer"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_computer;
    if (strcasecmp(name, "none"))
        return LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_none;
    return 0;
}

KINLINE const char *
SupportedJoinCredentialOwners_enum_to_name(LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_Enum value)
{
    switch(value) {
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_administrator:
        return "administrator";
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_user:
        return "user";
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_computer:
        return "computer";
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_none:
        return "none";
    default:
        return NULL;
    }
}

KINLINE LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_Enum
SupportedLeaveCredentialTypes_name_to_enum(const char *name)
{
    if (strcasecmp(name, "ccache"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_ccache;
    if (strcasecmp(name, "password"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_password;
    if (strcasecmp(name, "secrect"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_secrect;
    if (strcasecmp(name, "automatic"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_automatic;
    return 0;
}

KINLINE const char *SupportedLeaveCredentialTypes_enum_to_name(LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_Enum value)
{
    switch(value) {
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_ccache:
        return "ccache";
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_password:
        return "password";
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_secrect:
        return "secrect";
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_automatic:
        return "automatic";
    default:
        return NULL;
    }
}

KINLINE LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_Enum
SupportedLeaveCredentialOwners_name_to_enum(const char *name)
{
    if (strcasecmp(name, "administrator"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_administrator;
    if (strcasecmp(name, "user"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_user;
    if (strcasecmp(name, "computer"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_computer;
    if (strcasecmp(name, "none"))
        return LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_none;
    return 0;
}

KINLINE const char *
SupportedLeaveCredentialOwners_enum_to_name(LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_Enum value)
{
    switch(value) {
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_administrator:
        return "administrator";
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_user:
        return "user";
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_computer:
        return "computer";
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_none:
        return "none";
    default:
        return NULL;
    }
}

#define LMI_RealmdRealmInitKeys(klass, obj, dbus_path)                  \
{                                                                       \
    gchar *instance_id = NULL;                                          \
    const char *host_name = get_system_name();                          \
                                                                        \
    klass##_Init(obj, cb, ns);                                          \
                                                                        \
    instance_id = instance_id_from_dbus_path(dbus_path);                \
    klass##_Set_InstanceID(obj, instance_id);                           \
    g_free(instance_id);                                                \
                                                                        \
    klass##_Set_SystemCreationClassName(obj, get_system_creation_class_name()); \
    klass##_Set_SystemName(obj, host_name);                             \
}

KINLINE bool SupportsDBusInterface(GVariant *dbus_props, const char *dbus_interface)
{
    bool result = false;
    GVariantIter *iter = NULL;
    gchar *value;

    if (g_variant_lookup(dbus_props, "SupportedInterfaces", "as", &iter)) {
        while (g_variant_iter_next(iter, "&s", &value)) {
            if (strcmp(value, dbus_interface) == 0) {
                result = true;
                break;
            }
        }
        G_VARIANT_ITER_FREE(iter);
    }
    return result;
}

#define LMI_InitFromDBusRealmProps(klass, obj, dbus_props)              \
{                                                                       \
    gchar *value = NULL;                                                \
    gchar *name = NULL;                                                 \
    gsize n_items;                                                      \
    GVariantIter *iter;                                                 \
    CMPICount i;                                                        \
                                                                        \
    if (g_variant_lookup(dbus_props, "Name", "&s", &value)) {           \
        klass##_Set_RealmName(obj, value);                              \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "Configured", "&s", &value)) {     \
        if (strlen(value) == 0) {                                       \
            klass##_Null_Configured(obj);                               \
        } else {                                                        \
            char *interface_name = get_short_dbus_interface_name(value); \
            klass##_Set_Configured(obj, interface_name);                \
            g_free(interface_name);                                     \
        }                                                               \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "SupportedInterfaces", "as", &iter)) { \
        n_items = g_variant_iter_n_children(iter);                      \
        klass##_Init_SupportedInterfaces(obj, n_items);                 \
        for (i = 0; g_variant_iter_next(iter, "&s", &value); i++) {     \
            char *interface_name = get_short_dbus_interface_name(value); \
            klass##_Set_SupportedInterfaces(obj, i, interface_name);    \
            g_free(interface_name);                                     \
        }                                                               \
        G_VARIANT_ITER_FREE(iter);                                      \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "Details", "a(ss)", &iter)) {      \
        n_items = g_variant_iter_n_children(iter);                      \
        klass##_Init_DetailNames(obj, n_items);                         \
        klass##_Init_DetailValues(obj, n_items);                        \
        for (i = 0; g_variant_iter_next(iter, "(&s&s)", &name, &value); i++) { \
            klass##_Set_DetailNames(obj, i, name);                      \
            klass##_Set_DetailValues(obj, i, value);                    \
        }                                                               \
        G_VARIANT_ITER_FREE(iter);                                      \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "LoginFormats", "as", &iter)) {    \
        n_items = g_variant_iter_n_children(iter);                      \
        klass##_Init_LoginFormats(obj, n_items);                        \
        for (i = 0; g_variant_iter_next(iter, "&s", &value); i++) {     \
            klass##_Set_LoginFormats(obj, i, value);                    \
        }                                                               \
        G_VARIANT_ITER_FREE(iter);                                      \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "LoginPolicy", "&s", &value)) {    \
        klass##_Set_LoginPolicy(obj, value);                            \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "PermittedLogins", "as", &iter)) { \
        n_items = g_variant_iter_n_children(iter);                      \
        klass##_Init_PermittedLogins(obj, n_items);                     \
        for (i = 0; g_variant_iter_next(iter, "&s", &value); i++) {     \
            klass##_Set_PermittedLogins(obj, i, value);                 \
        }                                                               \
        G_VARIANT_ITER_FREE(iter);                                      \
    }                                                                   \
}

#define LMI_InitFromDBusKerberosRealmProps(klass, obj, dbus_props)      \
{                                                                       \
    gchar *value = NULL;                                                \
                                                                        \
    if (g_variant_lookup(dbus_props, "RealmName", "&s", &value)) {      \
        klass##_Set_RealmName(obj, value);                              \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "DomainName", "&s", &value)) {     \
        klass##_Set_DomainName(obj, value);                             \
    }                                                                   \
}

#define LMI_InitFromDBusKerberosMembershipProps(klass, obj, dbus_props) \
{                                                                       \
    gchar *value = NULL;                                                \
    gchar *type = NULL;                                                 \
    gchar *owner = NULL;                                                \
    gsize n_items;                                                      \
    GVariantIter *iter;                                                 \
    CMPICount i;                                                        \
                                                                        \
    if (g_variant_lookup(dbus_props, "SuggestedAdministrator", "&s", &value)) { \
        klass##_Set_SuggestedAdministrator(obj, value);                 \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "SupportedJoinCredentials", "a(ss)", &iter)) { \
        n_items = g_variant_iter_n_children(iter);                      \
        klass##_Init_SupportedJoinCredentialTypes(obj, n_items);        \
        klass##_Init_SupportedJoinCredentialOwners(obj, n_items);       \
        for (i = 0; g_variant_iter_next(iter, "(&s&s)", &type, &owner); i++) { \
            klass##_Set_SupportedJoinCredentialTypes(obj, i,            \
                                                     SupportedJoinCredentialTypes_name_to_enum(type)); \
            klass##_Set_SupportedJoinCredentialOwners(obj, i,           \
                                                     SupportedJoinCredentialOwners_name_to_enum(owner)); \
        }                                                               \
        G_VARIANT_ITER_FREE(iter);                                      \
    }                                                                   \
                                                                        \
    if (g_variant_lookup(dbus_props, "SupportedLeaveCredentials", "a(ss)", &iter)) { \
        n_items = g_variant_iter_n_children(iter);                      \
        klass##_Init_SupportedLeaveCredentialTypes(obj, n_items);       \
        klass##_Init_SupportedLeaveCredentialOwners(obj, n_items);      \
        for (i = 0; g_variant_iter_next(iter, "(&s&s)", &type, &owner); i++) { \
            klass##_Set_SupportedLeaveCredentialTypes(obj, i,           \
                                                     SupportedLeaveCredentialTypes_name_to_enum(type)); \
            klass##_Set_SupportedLeaveCredentialOwners(obj, i,          \
                                                     SupportedLeaveCredentialOwners_name_to_enum(owner)); \
        }                                                               \
        G_VARIANT_ITER_FREE(iter);                                      \
    }                                                                   \
}

CMPIStatus LMI_RealmdRealmRef_InitFromDBusPath(
    LMI_RealmdRealmRef* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path);


CMPIStatus LMI_RealmdRealm_InitFromDBusPath(
    LMI_RealmdRealm* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path);

CMPIStatus LMI_RealmdKerberosRealmRef_InitFromDBusPath(
    LMI_RealmdKerberosRealmRef* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path);

CMPIStatus LMI_RealmdKerberosRealm_InitFromDBusPath(
    LMI_RealmdKerberosRealm* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path);

#endif /* __RDCP_REALMDREALM_H__ */
