#include <konkret/konkret.h>
#include "LMI_RealmdKerberosRealm.h"
#include "globals.h"
#include "rdcp_error.h"
#include "rdcp_dbus.h"
#include "rdcp_util.h"
#include "rdcp_realmdrealm.h"

static const CMPIBroker* _cb = NULL;

CMPIStatus LMI_RealmdKerberosRealmRef_InitFromDBusPath(
    LMI_RealmdKerberosRealmRef* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path)
{
    CMPIStatus status;

    CMSetStatus(&status, CMPI_RC_OK);

    LMI_RealmdRealmInitKeys(LMI_RealmdKerberosRealmRef, self, dbus_path);

    return status;
}

CMPIStatus LMI_RealmdKerberosRealm_InitFromDBusPath(
    LMI_RealmdKerberosRealm* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path)
{
    CMPIStatus status;
    GError *g_error = NULL;
    GVariant *realm_props = NULL;
    GVariant *kerberos_props = NULL;
    GVariant *kerberos_membership_props = NULL;

    CMSetStatus(&status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        return handle_g_error(&g_error, _cb, &status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
    }

    GET_DBUS_PROPERIES_OR_EXIT(realm_props, dbus_path,
                               REALM_DBUS_REALM_INTERFACE, &status);
    GET_DBUS_PROPERIES_OR_EXIT(kerberos_props, dbus_path,
                               REALM_DBUS_KERBEROS_INTERFACE, &status);

    LMI_RealmdRealmInitKeys(LMI_RealmdKerberosRealm, self, dbus_path);
    LMI_InitFromDBusRealmProps(LMI_RealmdKerberosRealm, self, realm_props);
    LMI_InitFromDBusKerberosRealmProps(LMI_RealmdKerberosRealm, self, kerberos_props);

    if (SupportsDBusInterface(realm_props, REALM_DBUS_KERBEROS_MEMBERSHIP_INTERFACE)) {
        GET_DBUS_PROPERIES_OR_EXIT(kerberos_membership_props, dbus_path,
                                   REALM_DBUS_KERBEROS_MEMBERSHIP_INTERFACE, &status);

        LMI_InitFromDBusKerberosMembershipProps(LMI_RealmdKerberosRealm, self,
                                                kerberos_membership_props);
    }

 exit:
    G_VARIANT_FREE(realm_props);
    G_VARIANT_FREE(kerberos_props);
    G_VARIANT_FREE(kerberos_membership_props);

    return status;
}


static void LMI_RealmdKerberosRealmInitialize()
{
}

static CMPIStatus LMI_RealmdKerberosRealmCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_RealmdKerberosRealmEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus LMI_RealmdKerberosRealmEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    CMPIStatus status;
    GError *g_error = NULL;
    GVariant *provider_props = NULL;
    GVariant *realm_props = NULL;
    GVariantIter *iter = NULL;
    gchar *realm_obj_path;
    const char *name_space = KNameSpace(cop);

    CMSetStatus(&status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        return handle_g_error(&g_error, _cb, &status, CMPI_RC_ERR_FAILED,
                              "rdcp_dbus_initialize failed");
    }

    GET_DBUS_PROPERIES_OR_EXIT(provider_props, REALM_DBUS_SERVICE_PATH,
                               REALM_DBUS_PROVIDER_INTERFACE, &status);

    g_variant_lookup(provider_props, "Realms", "ao", &iter);
    while (g_variant_iter_next(iter, "&o", &realm_obj_path)) {
        LMI_RealmdKerberosRealm realmd_realm;

        GET_DBUS_PROPERIES_OR_EXIT(realm_props, realm_obj_path,
                                   REALM_DBUS_REALM_INTERFACE, &status);
        if (!SupportsDBusInterface(realm_props, REALM_DBUS_KERBEROS_INTERFACE)) {
            G_VARIANT_FREE(realm_props);
            continue;
        }
        G_VARIANT_FREE(realm_props);

        status = LMI_RealmdKerberosRealm_InitFromDBusPath(&realmd_realm, _cb,
                                                          name_space, realm_obj_path);
        if (status.rc != CMPI_RC_OK) {
            goto exit;
        }

        KReturnInstance(cr, realmd_realm);
    }

 exit:
    G_VARIANT_ITER_FREE(iter);
    G_VARIANT_FREE(provider_props);
    G_VARIANT_FREE(realm_props);

    return status;
}

static CMPIStatus LMI_RealmdKerberosRealmGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus LMI_RealmdKerberosRealmCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_RealmdKerberosRealmModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_RealmdKerberosRealmDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_RealmdKerberosRealmExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    LMI_RealmdKerberosRealm,
    LMI_RealmdKerberosRealm,
    _cb,
    LMI_RealmdKerberosRealmInitialize())

static CMPIStatus LMI_RealmdKerberosRealmMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_RealmdKerberosRealmInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return LMI_RealmdKerberosRealm_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    LMI_RealmdKerberosRealm,
    LMI_RealmdKerberosRealm,
    _cb,
    LMI_RealmdKerberosRealmInitialize())

KUint32 LMI_RealmdKerberosRealm_ChangeLoginPolicy(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const LMI_RealmdKerberosRealmRef* self,
    const KString* LoginPolicy,
    const KStringA* PermittedAdd,
    const KStringA* PermittedRemove,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KUint32 LMI_RealmdKerberosRealm_Deconfigure(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const LMI_RealmdKerberosRealmRef* self,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KEXTERN KUint32 LMI_RealmdKerberosRealm_Join(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const LMI_RealmdKerberosRealmRef* self,
    const KUint32* Type,
    const KUint32* Owner,
    const KString* Name,
    const KString* Password,
    const KUint8A* Data,
    const KStringA* OptionNames,
    const KStringA* OptionValues,
    CMPIStatus* status)
{
    GError *g_error = NULL;
    KUint32 result = KUINT32_INIT;
    const char *cred_type = NULL;
    const char *cred_owner = NULL;
    gchar *data = NULL;
    gsize data_len;
    gchar *dbus_path = NULL;
    GVariant *credentials = NULL;
    GVariant *data_variant = NULL;
    GVariant *options = NULL;

    KUint32_Set(&result, LMI_REALMD_RESULT_SUCCESS);
    CMSetStatus(status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        handle_g_error(&g_error, _cb, status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!Type->exists || Type->null) {
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Type parameter absent");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!Owner->exists || Owner->null) {
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Owner parameter absent");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!dbus_path_from_instance_id(self->InstanceID.chars, &dbus_path, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "dbus_path_from_instance_id() failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    switch(Owner->value) {
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_administrator:
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_user:
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_computer:
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialOwners_none:
        break;
    default:
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Invalid Owner parameter");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    cred_type = SupportedJoinCredentialTypes_enum_to_name(Type->value);
    cred_owner = SupportedJoinCredentialOwners_enum_to_name(Owner->value);

    switch(Type->value) {
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_ccache:
        if ((Name->exists && !Name->null) || (Password->exists && !Password->null)) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name & Password parameters must be NULL when Type is ccache");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
        if (!Data->exists || Data->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Data parameter must be provided when Type is ccache");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

        if ((data = get_data_from_KUint8A(Data, &data_len)) == NULL) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_FAILED,
                                 "unabled to allocate memory");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
	data_variant = g_variant_new_from_data(G_VARIANT_TYPE ("ay"),
                                               data, data_len,
                                               TRUE, g_free, (gpointer) data);

        credentials = g_variant_new("(ssv)", cred_type, cred_owner, data_variant);
        break;
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_password:
        if (!Name->exists || Name->null || !Password->exists || Password->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name & Password parameters must be provided when Type is password");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
        if (Data->exists && !Data->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Data parameter must be NULL when Type is password");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

        credentials = g_variant_new("(ssv)", cred_type, cred_owner,
                                    g_variant_new("(ss)", Name->chars, Password->chars));

        break;
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_secrect:
        if ((Name->exists && !Name->null) || (Password->exists && !Password->null)) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name & Password parameters must be NULL when Type is secret");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
        if (!Data->exists || Data->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Data parameter must be provided when Type is secret");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

        if ((data = get_data_from_KUint8A(Data, &data_len)) == NULL) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_FAILED,
                                 "unabled to allocate memory");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
	credentials = g_variant_new("(ssv)", cred_type, cred_owner,
                                    g_variant_new_fixed_array(G_VARIANT_TYPE_BYTE,
                                                              data, data_len, 1));
        break;
    case LMI_RealmdKerberosRealm_SupportedJoinCredentialTypes_automatic:
        if ((Name->exists && !Name->null) || (Password->exists && !Password->null) ||
            (Data->exists && !Data->null)) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name, Password & Data parameters must be NULL when Type is secret");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

	credentials = g_variant_new ("(ssv)", cred_type, cred_owner,
                                     g_variant_new_string (""));

        break;
    default:
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Invalid Type parameter");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }


    if (!build_g_variant_options_from_KStringA(OptionNames, OptionValues, &options, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "failed to convert options to gvariant");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!dbus_join_call(system_bus, dbus_path, credentials, options, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED, "dbus_join_call() failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }


 exit:

    g_free(data);
    G_VARIANT_FREE(credentials);
    G_VARIANT_FREE(data_variant);
    G_VARIANT_FREE(options);

    return result;
}

KEXTERN KUint32 LMI_RealmdKerberosRealm_Leave(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const LMI_RealmdKerberosRealmRef* self,
    const KUint32* Type,
    const KUint32* Owner,
    const KString* Name,
    const KString* Password,
    const KUint8A* Data,
    const KStringA* OptionNames,
    const KStringA* OptionValues,
    CMPIStatus* status)
{
    GError *g_error = NULL;
    KUint32 result = KUINT32_INIT;
    const char *cred_type = NULL;
    const char *cred_owner = NULL;
    gchar *data = NULL;
    gsize data_len;
    gchar *dbus_path = NULL;
    GVariant *credentials = NULL;
    GVariant *data_variant = NULL;
    GVariant *options = NULL;

    KUint32_Set(&result, LMI_REALMD_RESULT_SUCCESS);
    CMSetStatus(status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        handle_g_error(&g_error, _cb, status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!Type->exists || Type->null) {
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Type parameter absent");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!Owner->exists || Owner->null) {
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Owner parameter absent");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!dbus_path_from_instance_id(self->InstanceID.chars, &dbus_path, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "dbus_path_from_instance_id() failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    switch(Owner->value) {
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_administrator:
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_user:
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_computer:
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialOwners_none:
        break;
    default:
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Invalid Owner parameter");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    cred_type = SupportedLeaveCredentialTypes_enum_to_name(Type->value);
    cred_owner = SupportedLeaveCredentialOwners_enum_to_name(Owner->value);

    switch(Type->value) {
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_ccache:
        if ((Name->exists && !Name->null) || (Password->exists && !Password->null)) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name & Password parameters must be NULL when Type is ccache");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
        if (!Data->exists || Data->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Data parameter must be provided when Type is ccache");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

        if ((data = get_data_from_KUint8A(Data, &data_len)) == NULL) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_FAILED,
                                 "unabled to allocate memory");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
	data_variant = g_variant_new_from_data(G_VARIANT_TYPE ("ay"),
                                               data, data_len,
                                               TRUE, g_free, (gpointer) data);

        credentials = g_variant_new("(ssv)", cred_type, cred_owner, data_variant);
        break;
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_password:
        if (!Name->exists || Name->null || !Password->exists || Password->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name & Password parameters must be provided when Type is password");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
        if (Data->exists && !Data->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Data parameter must be NULL when Type is password");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

        credentials = g_variant_new("(ssv)", cred_type, cred_owner,
                                    g_variant_new("(ss)", Name->chars, Password->chars));

        break;
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_secrect:
        if ((Name->exists && !Name->null) || (Password->exists && !Password->null)) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name & Password parameters must be NULL when Type is secret");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
        if (!Data->exists || Data->null) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Data parameter must be provided when Type is secret");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

        if ((data = get_data_from_KUint8A(Data, &data_len)) == NULL) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_FAILED,
                                 "unabled to allocate memory");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }
	credentials = g_variant_new("(ssv)", cred_type, cred_owner,
                                    g_variant_new_fixed_array(G_VARIANT_TYPE_BYTE,
                                                              data, data_len, 1));
        break;
    case LMI_RealmdKerberosRealm_SupportedLeaveCredentialTypes_automatic:
        if ((Name->exists && !Name->null) || (Password->exists && !Password->null) ||
            (Data->exists && !Data->null)) {
            CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER,
                                 "Name, Password & Data parameters must be NULL when Type is secret");
            KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
            goto exit;
        }

	credentials = g_variant_new ("(ssv)", cred_type, cred_owner,
                                     g_variant_new_string (""));

        break;
    default:
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "Invalid Type parameter");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }


    if (!build_g_variant_options_from_KStringA(OptionNames, OptionValues, &options, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "failed to convert options to gvariant");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!dbus_leave_call(system_bus, dbus_path, credentials, options, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED, "dbus_leave_call() failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }


 exit:

    g_free(data);
    G_VARIANT_FREE(credentials);
    G_VARIANT_FREE(data_variant);
    G_VARIANT_FREE(options);

    return result;
}


KONKRET_REGISTRATION(
    "root/cimv2",
    "LMI_RealmdKerberosRealm",
    "LMI_RealmdKerberosRealm",
    "instance method");
