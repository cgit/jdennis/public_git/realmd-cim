#include <konkret/konkret.h>
#include "LMI_ServiceAffectsRealmdRealm.h"
#include "rdcp_util.h"
#include "globals.h"
#include "rdcp_error.h"
#include "rdcp_dbus.h"
#include "rdcp_util.h"
#include "rdcp_realmdrealm.h"

static const CMPIBroker* _cb;

static void LMI_ServiceAffectsRealmdRealmInitialize()
{
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    CMPIStatus status;
    GError *g_error = NULL;
    GVariant *provider_props = NULL;
    GVariantIter *iter = NULL;
    gchar *realm_obj_path;
    LMI_RealmdServiceRef realmd_service_ref;
    LMI_ServiceAffectsRealmdRealm service_affects;
    const char *name_space = KNameSpace(cop);
    const char *host_name = get_system_name();

    CMSetStatus(&status, CMPI_RC_OK);

    LMI_InitRealmdServiceKeys(LMI_RealmdServiceRef, &realmd_service_ref, name_space, host_name);

    if (!rdcp_dbus_initialize(&g_error)) {
        return handle_g_error(&g_error, _cb, &status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
    }

    GET_DBUS_PROPERIES_OR_EXIT(provider_props, REALM_DBUS_SERVICE_PATH,
                               REALM_DBUS_PROVIDER_INTERFACE, &status);

    g_variant_lookup(provider_props, "Realms", "ao", &iter);
    while (g_variant_iter_next(iter, "&o", &realm_obj_path)) {
        LMI_RealmdRealmRef realmd_realm_ref;

        status = LMI_RealmdRealmRef_InitFromDBusPath(&realmd_realm_ref, _cb, name_space, realm_obj_path);
        if (status.rc != CMPI_RC_OK) {
            goto exit;
        }

        LMI_ServiceAffectsRealmdRealm_Init(&service_affects, _cb, name_space);
        LMI_ServiceAffectsRealmdRealm_Set_AffectedElement(&service_affects, &realmd_realm_ref);
        LMI_ServiceAffectsRealmdRealm_Set_AffectingElement(&service_affects, &realmd_service_ref);
        LMI_ServiceAffectsRealmdRealm_Init_ElementEffects(&service_affects, 1);
        LMI_ServiceAffectsRealmdRealm_Set_ElementEffects(&service_affects, 0,
            LMI_ServiceAffectsRealmdRealm_ElementEffects_Manages);

        KReturnInstance(cr, service_affects);
    }

 exit:
    G_VARIANT_ITER_FREE(iter);
    G_VARIANT_FREE(provider_props);

    return status;
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    return KDefaultGetInstance(
        _cb, mi, cc, cr, cop, properties);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char**properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmAssociationCleanup(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmAssociators(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole,
    const char** properties)
{
    return KDefaultAssociators(
        _cb,
        mi,
        cc,
        cr,
        cop,
        LMI_ServiceAffectsRealmdRealm_ClassName,
        assocClass,
        resultClass,
        role,
        resultRole,
        properties);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmAssociatorNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* resultClass,
    const char* role,
    const char* resultRole)
{
    return KDefaultAssociatorNames(
        _cb,
        mi,
        cc,
        cr,
        cop,
        LMI_ServiceAffectsRealmdRealm_ClassName,
        assocClass,
        resultClass,
        role,
        resultRole);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmReferences(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role,
    const char** properties)
{
    return KDefaultReferences(
        _cb,
        mi,
        cc,
        cr,
        cop,
        LMI_ServiceAffectsRealmdRealm_ClassName,
        assocClass,
        role,
        properties);
}

static CMPIStatus LMI_ServiceAffectsRealmdRealmReferenceNames(
    CMPIAssociationMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* assocClass,
    const char* role)
{
    return KDefaultReferenceNames(
        _cb,
        mi,
        cc,
        cr,
        cop,
        LMI_ServiceAffectsRealmdRealm_ClassName,
        assocClass,
        role);
}

CMInstanceMIStub(
    LMI_ServiceAffectsRealmdRealm,
    LMI_ServiceAffectsRealmdRealm,
    _cb,
    LMI_ServiceAffectsRealmdRealmInitialize())

CMAssociationMIStub(
    LMI_ServiceAffectsRealmdRealm,
    LMI_ServiceAffectsRealmdRealm,
    _cb,
    LMI_ServiceAffectsRealmdRealmInitialize())

KONKRET_REGISTRATION(
    "root/cimv2",
    "LMI_ServiceAffectsRealmdRealm",
    "LMI_ServiceAffectsRealmdRealm",
    "instance association");
