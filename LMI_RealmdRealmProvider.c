#include <konkret/konkret.h>
#include <string.h>
#include "LMI_RealmdRealm.h"
#include "globals.h"
#include "rdcp_error.h"
#include "rdcp_dbus.h"
#include "rdcp_util.h"
#include "rdcp_realmdrealm.h"

static const CMPIBroker* _cb = NULL;

CMPIStatus LMI_RealmdRealmRef_InitFromDBusPath(
    LMI_RealmdRealmRef* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path)
{
    CMPIStatus status;

    CMSetStatus(&status, CMPI_RC_OK);

    LMI_RealmdRealmInitKeys(LMI_RealmdRealmRef, self, dbus_path);

    return status;
}

CMPIStatus LMI_RealmdRealm_InitFromDBusPath(
    LMI_RealmdRealm* self,
    const CMPIBroker* cb,
    const char* ns,
    const char* dbus_path)
{
    CMPIStatus status;
    GError *g_error = NULL;
    GVariant *realm_props = NULL;

    CMSetStatus(&status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        return handle_g_error(&g_error, _cb, &status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
    }

    GET_DBUS_PROPERIES_OR_EXIT(realm_props, dbus_path,
                               REALM_DBUS_REALM_INTERFACE, &status);

    LMI_RealmdRealmInitKeys(LMI_RealmdRealm, self, dbus_path);
    LMI_InitFromDBusRealmProps(LMI_RealmdRealm, self, realm_props);

 exit:
    G_VARIANT_FREE(realm_props);

    return status;
}

static void LMI_RealmdRealmInitialize()
{
}

static CMPIStatus LMI_RealmdRealmCleanup(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_RealmdRealmEnumInstanceNames(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    return KDefaultEnumerateInstanceNames(
        _cb, mi, cc, cr, cop);
}

static CMPIStatus LMI_RealmdRealmEnumInstances(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    CMPIStatus status;
    GError *g_error = NULL;
    GVariant *provider_props = NULL;
    GVariantIter *iter = NULL;
    gchar *realm_obj_path;
    const char *name_space = KNameSpace(cop);

    CMSetStatus(&status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        return handle_g_error(&g_error, _cb, &status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
    }

    GET_DBUS_PROPERIES_OR_EXIT(provider_props, REALM_DBUS_SERVICE_PATH,
                               REALM_DBUS_PROVIDER_INTERFACE, &status);

    g_variant_lookup(provider_props, "Realms", "ao", &iter);
    while (g_variant_iter_next(iter, "&o", &realm_obj_path)) {
        LMI_RealmdRealm realmd_realm;

        status = LMI_RealmdRealm_InitFromDBusPath(&realmd_realm, _cb,
                                                  name_space, realm_obj_path);
        if (status.rc != CMPI_RC_OK) {
            goto exit;
        }
        KReturnInstance(cr, realmd_realm);
    }

 exit:
    G_VARIANT_ITER_FREE(iter);
    G_VARIANT_FREE(provider_props);

    return status;
}

static CMPIStatus LMI_RealmdRealmGetInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char** properties)
{
    CMPIStatus status;
    GError *g_error = NULL;
    LMI_RealmdRealmRef realmdrealm_ref;
    gchar *dbus_path = NULL;
    LMI_RealmdRealm realmd_realm;

    CMSetStatus(&status, CMPI_RC_OK);

    KReturnIf(LMI_RealmdRealmRef_InitFromObjectPath(&realmdrealm_ref, _cb, cop));

    if (!dbus_path_from_instance_id(realmdrealm_ref.InstanceID.chars, &dbus_path, &g_error)) {
        return handle_g_error(&g_error, _cb, &status, CMPI_RC_ERR_FAILED,
                              "dbus_path_from_instance_id() failed");
    }

    KReturnIf(LMI_RealmdRealm_InitFromDBusPath(&realmd_realm, _cb, KNameSpace(cop), dbus_path));

    KReturnInstance(cr, realmd_realm);

    return status;
}

static CMPIStatus LMI_RealmdRealmCreateInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_RealmdRealmModifyInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const CMPIInstance* ci,
    const char** properties)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_RealmdRealmDeleteInstance(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

static CMPIStatus LMI_RealmdRealmExecQuery(
    CMPIInstanceMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* lang,
    const char* query)
{
    CMReturn(CMPI_RC_ERR_NOT_SUPPORTED);
}

CMInstanceMIStub(
    LMI_RealmdRealm,
    LMI_RealmdRealm,
    _cb,
    LMI_RealmdRealmInitialize())

static CMPIStatus LMI_RealmdRealmMethodCleanup(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    CMPIBoolean term)
{
    CMReturn(CMPI_RC_OK);
}

static CMPIStatus LMI_RealmdRealmInvokeMethod(
    CMPIMethodMI* mi,
    const CMPIContext* cc,
    const CMPIResult* cr,
    const CMPIObjectPath* cop,
    const char* meth,
    const CMPIArgs* in,
    CMPIArgs* out)
{
    return LMI_RealmdRealm_DispatchMethod(
        _cb, mi, cc, cr, cop, meth, in, out);
}

CMMethodMIStub(
    LMI_RealmdRealm,
    LMI_RealmdRealm,
    _cb,
    LMI_RealmdRealmInitialize())

KUint32 LMI_RealmdRealm_ChangeLoginPolicy(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const LMI_RealmdRealmRef* self,
    const KString* LoginPolicy,
    const KStringA* PermittedAdd,
    const KStringA* PermittedRemove,
    CMPIStatus* status)
{
    GError *g_error = NULL;
    KUint32 result = KUINT32_INIT;
    gchar *dbus_path = NULL;
    const gchar *login_policy = NULL;
    GVariant *permitted_add = NULL;
    GVariant *permitted_remove = NULL;
    GVariant *options = NULL;

    KUint32_Set(&result, LMI_REALMD_RESULT_SUCCESS);
    CMSetStatus(status, CMPI_RC_OK);

    if (!rdcp_dbus_initialize(&g_error)) {
        handle_g_error(&g_error, _cb, status, CMPI_RC_ERR_FAILED, "rdcp_dbus_initialize failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!LoginPolicy->exists || LoginPolicy->null) {
        login_policy = "";
    } else {
        login_policy = LoginPolicy->chars;
    }

    if (!PermittedAdd->exists || PermittedAdd->null) {
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "PermittedAdd parameter absent");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!PermittedRemove->exists || PermittedRemove->null) {
        CMSetStatusWithChars(cb, status, CMPI_RC_ERR_INVALID_PARAMETER, "PermittedRemove parameter absent");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!dbus_path_from_instance_id(self->InstanceID.chars, &dbus_path, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "dbus_path_from_instance_id() failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!build_g_variant_string_array_from_KStringA(PermittedAdd, &permitted_add, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "failed to convert PermittedAdd to gvariant array");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    if (!build_g_variant_string_array_from_KStringA(PermittedRemove, &permitted_remove, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED,
                       "failed to convert PermittedRemove to gvariant array");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }

    /* For now we don't pass any options so just create an empty dictionary */
    options =  g_variant_new_array(G_VARIANT_TYPE ("{sv}"), NULL, 0);

    if (!dbus_change_login_policy_call(system_bus, dbus_path, login_policy,
                                       permitted_add, permitted_remove,
                                       options, &g_error)) {
        handle_g_error(&g_error, cb, status, CMPI_RC_ERR_FAILED, "dbus_change_login_policy_call() failed");
        KUint32_Set(&result, LMI_REALMD_RESULT_FAILED);
        goto exit;
    }


 exit:

    G_VARIANT_FREE(permitted_add);
    G_VARIANT_FREE(permitted_remove);
    G_VARIANT_FREE(options);

    return result;
}

KUint32 LMI_RealmdRealm_Deconfigure(
    const CMPIBroker* cb,
    CMPIMethodMI* mi,
    const CMPIContext* context,
    const LMI_RealmdRealmRef* self,
    CMPIStatus* status)
{
    KUint32 result = KUINT32_INIT;

    KSetStatus(status, ERR_NOT_SUPPORTED);
    return result;
}

KONKRET_REGISTRATION(
    "root/cimv2",
    "LMI_RealmdRealm",
    "LMI_RealmdRealm",
    "instance method");
