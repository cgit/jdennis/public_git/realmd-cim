# Barebones spec file, mostly used for testing at this point.
# To produce a tar file run tools/make-dist

Name:           realmd-cim
Version:        0.0.0
Release:        1%{?dist}
Summary:        openlmi CIM provider for realmd

License:        GPLv2+
URL:            git://fedorapeople.org/~jdennis/realmd-cim.git
Source0:        http://jdennis.fedorapeople.org/realmd-cim-%{version}.tar.gz

BuildRequires:  cmake
BuildRequires:  openlmi-providers-devel
BuildRequires:  konkretcmpi-devel
BuildRequires:  sblim-cmpi-devel
BuildRequires:  cim-schema
BuildRequires:  dbus-devel
BuildRequires:  glib2-devel

Requires:       realmd
Requires:       cim-server

%description
%{name} exposes domain joining as a CIM provider via the realmd service


%prep
%setup -q


%build
mkdir -p %{_target_platform}
pushd %{_target_platform}
%{cmake} ..
popd

make %{?_smp_mflags} -C %{_target_platform}

%install
make install/fast DESTDIR=$RPM_BUILD_ROOT -C %{_target_platform}

%pre
# If upgrading, deregister old version
if [ "$1" -gt 1 ]; then
    %{_bindir}/openlmi-mof-register unregister \
        %{_datadir}/%{name}/LMI_Realmd.mof \
        %{_datadir}/%{name}/LMI_Realmd.reg \
        > /dev/null 2>&1 || :;
fi

%post
/sbin/ldconfig
# Register Schema and Provider
if [ "$1" -ge 1 ]; then
    %{_bindir}/openlmi-mof-register register \
        %{_datadir}/%{name}/LMI_Realmd.mof \
        %{_datadir}/%{name}/LMI_Realmd.reg \
        > /dev/null 2>&1 || :;
fi

%preun
# Deregister only if not upgrading
if [ "$1" -eq 0 ]; then
    %{_bindir}/openlmi-mof-register unregister \
        %{_datadir}/%{name}/LMI_Realmd.mof \
        %{_datadir}/%{name}/LMI_Realmd.reg \
        > /dev/null 2>&1 || :;
fi

%postun -p /sbin/ldconfig

%files
%doc README LICENSE VERSION
%{_libdir}/cmpi/libcmpiLMI_Realmd.so
%{_datadir}/openlmi-providers/LMI_Realmd.mof
%{_datadir}/openlmi-providers/LMI_Realmd.reg



%changelog
